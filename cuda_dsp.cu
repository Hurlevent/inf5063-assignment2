#include <inttypes.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cuda_fp16.h>
#include <math_functions.h>
extern "C"{
#include "cuda_dsp.h"
#include "tables.h"
}
//constant memory veriables
__device__ __constant__  int zigzagBlock[64] = { 0, 1, 8, 16, 9, 2, 3, 10, 17, 24, 32, 25, 18, 11, 4, 5, 12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6, 7, 14, 21, 28, 35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51, 58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63 };

__device__ __constant__ float dev_dctlookup[8][8] =
{
	{ 1.0f, 0.980785f, 0.923880f, 0.831470f, 0.707107f, 0.555570f, 0.382683f, 0.195090f, },
	{ 1.0f, 0.831470f, 0.382683f, -0.195090f, -0.707107f, -0.980785f, -0.923880f, -0.555570f, },
	{ 1.0f, 0.555570f, -0.382683f, -0.980785f, -0.707107f, 0.195090f, 0.923880f, 0.831470f, },
	{ 1.0f, 0.195090f, -0.923880f, -0.555570f, 0.707107f, 0.831470f, -0.382683f, -0.980785f, },
	{ 1.0f, -0.195090f, -0.923880f, 0.555570f, 0.707107f, -0.831470f, -0.382683f, 0.980785f, },
	{ 1.0f, -0.555570f, -0.382683f, 0.980785f, -0.707107f, -0.195090f, 0.923880f, -0.831470f, },
	{ 1.0f, -0.831470f, 0.382683f, 0.195090f, -0.707107f, 0.980785f, -0.923880f, 0.555570f, },
	{ 1.0f, -0.980785f, 0.923880f, -0.831470f, 0.707107f, -0.555570f, 0.382683f, -0.195090f, },
};

__device__ static void gpu_transpose_block(__half *in_data, __half *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    for (j = 0; j < 8; ++j)
    {
      out_data[i*8+j] = in_data[j*8+i];
    }
  }
}

__device__ static void gpu_dct_1d(__half *in_data, __half *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
	  __half dct = __int2half_rz(0);

    for (j = 0; j < 8; ++j)
    {
		dct = __hadd(dct, __hmul(in_data[j], __float2half(dev_dctlookup[j][i])));
    }

    out_data[i] = dct;
  }
}

__device__ static void gpu_idct_1d(__half *in_data, __half *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
	  __half idct = __int2half_rz(0);

    for (j = 0; j < 8; ++j)
    {
		idct = __hadd(idct, __hmul(in_data[j], __float2half(dev_dctlookup[i][j])));
    }

    out_data[i] = idct;
  }
}

__device__ static void gpu_scale_block(__half *in_data, __half *out_data)
{
  int u, v;

  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
		float a1 = !u ? ISQRT2 : 1.0f;
		float a2 = !v ? ISQRT2 : 1.0f;
		__half a12 = __hmul(__float2half(a1), __float2half(a2));
      /* Scale according to normalizing function */
		out_data[v * 8 + u] = __hmul(in_data[v * 8 + u], a12);
    }
  }
}

__device__ static void gpu_quantize_block(__half *in_data, __half *out_data, uint8_t *quant_tbl)
{
	int zigzag;

	for (zigzag = 0; zigzag < 64; ++zigzag)
	{
		__half dct = in_data[zigzagBlock[zigzag]];
		/* Zig-zag and quantize */
		out_data[zigzag] = hrint(hdiv(hdiv(dct, __int2half_rz(4)), __int2half_rz(quant_tbl[zigzag])));
	}
}

__device__ static void gpu_dequantize_block(__half *in_data, __half *out_data,
    uint8_t *quant_tbl)
{
	int zigzag;

	for (zigzag = 0; zigzag < 64; ++zigzag)
	{
		__half dct = in_data[zigzag];
		/* Zig-zag and de-quantize */
		out_data[zigzagBlock[zigzag]] = hrint(hdiv(__hmul(dct, __int2half_rz(quant_tbl[zigzag])), __int2half_rz(4)));
	}
}

__device__ int floata2int(double d)
{
	union Cast
	{
		double d;
		long l;
	};
	volatile Cast c;
	c.d = d + 6755399441055744.0;
	return c.l;
}
// Has definition in header
__device__ void gpu_dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
	__half mb[8 * 8]  __attribute((aligned(16)));
	__half mb2[8 * 8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) {
	  mb2[i] = __short2half_rz(in_data[i]);
  }
  
  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { gpu_dct_1d(mb2+v*8, mb+v*8); }
  gpu_transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { gpu_dct_1d(mb2+v*8, mb+v*8); }
  gpu_transpose_block(mb, mb2);
  
  gpu_scale_block(mb2, mb);
  
  gpu_quantize_block(mb, mb2, quant_tbl);
  
  for (i = 0; i < 64; ++i) { 
	  out_data[i] = __half2short_rz(mb2[i]);
  }
}

// Has definition in header
__device__ void gpu_dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
	__half mb[8 * 8] __attribute((aligned(16)));
	__half mb2[8 * 8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb[i] = __short2half_rz(in_data[i]); }

  gpu_dequantize_block(mb, mb2, quant_tbl);
  gpu_scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  for (v = 0; v < 8; ++v) { gpu_idct_1d(mb+v*8, mb2+v*8); }
  gpu_transpose_block(mb2, mb);
  for (v = 0; v < 8; ++v) { gpu_idct_1d(mb+v*8, mb2+v*8); }
  gpu_transpose_block(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = __half2short_rz(mb[i]); }
}
