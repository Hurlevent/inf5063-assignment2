#!/bin/bash

rm a.out
rm *.o
nvcc -ccbin g++ -m64 -gencode arch=compute_53,code=sm_53 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_62,code=sm_62 -gencode arch=compute_62,code=compute_62 -c *.cu
gcc -c common.c c63enc.c dsp.c io.c me.c tables.c c63_write.c
nvcc -ccbin g++ -m64 -gencode arch=compute_53,code=sm_53 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_62,code=sm_62 -gencode arch=compute_62,code=compute_62 *.o

echo "There should now be an a.out file, if everyting went according to plan"
