#!/bin/bash


if [ $# -eq 0 ]; then
echo "Encoding foreman with cuda version"
./c63enc -w 352 -h 288 -o output.y4m foreman.yuv
elif [ $1 == "nvprof" ]; then
echo "Using nvprof while encoding foreman.yuv"
nvprof ./c63enc -w 352 -h 288 -o output.y4m foreman.yuv
elif [ $1 == "memcheck" ]; then
cuda-memcheck ./c63enc -w 352 -h 288 -o output.y4m foreman.yuv
elif [ $1 == "no_cuda" ]; then
echo "Encoding foreman with cudaless version"
./c63enc_no_cuda -w 352 -h 288 -o output.y4m foreman.yuv
elif [ $1 == "help" ]; then
echo "Usage:"
echo "Encode foreman with cuda version"
echo "Required files: c63enc foreman.yuv"
echo "./runforeman.sh"
echo ""
echo "Use nvprof when encoding foreman"
echo "Required files: c63enc foreman.yuv"
echo "./runforeman.sh nvprof"
echo ""
echo "Encode foreman with cudaless version"
echo "Required files: c63enc_no_cuda foreman.yuv"
echo "./runforeman.sh no_cuda"
echo ""
echo "To generate the binary files, you can type:"
echo ""
echo "make"
echo ""
echo "Which will generate the c63enc binary (as well as the c63dec and c63pred)"
echo "In order to generate the cudaless version, you need to type:"
echo ""
echo "make c63enc_no_cuda"
echo ""
echo "Decode the encoded video file"
echo "Required files: c63dec output.y4m"
echo "./runforeman.sh decode"
echo ""
elif [ $1 == "decode" ]; then
echo "Decoding output.y4m"
echo "Output file: decoded.y4m"
./c63dec output.y4m decoded.y4m
fi
