#include <cuda_runtime.h>
#include <stdio.h>

#define CUDA_CHECK_ERROR(msg) \
  do { \
    cudaError_t __err = cudaGetLastError(); \
    if (__err != cudaSuccess) { \
      fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
	      msg, cudaGetErrorString(__err), \
	      __FILE__, __LINE__); \
      fprintf(stderr, "*** FAILED - ABORTING\n"); \
      exit(1); \
    } \
  } while (0)

extern "C" void test_gpu();

__global__ void kernel_double(int * out, int * in){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  out[idx] = in[idx] * 2;
}

void gpu_print_properties(){
  int nDevices;

  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
	   prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
	   prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
	   2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  } 
}

void test_gpu(){
  cudaError_t err = cudaSuccess;

  gpu_print_properties();
  
  printf("Performing simple cuda test\n");
  
  int count = 10;
  
  int * d_a = NULL;
  int * d_b = NULL;
  int * h_a = NULL;
  int * h_b = NULL;
    
  h_a = (int *)calloc(count, sizeof(int));
  h_b = (int *)calloc(count, sizeof(int));
  
  for(int i = 0; i < count; ++i){
    h_a[i] = i;
    h_b[i] = 0;
  }

  for(int i = 0; i < count; ++i){
    printf(" %i ", h_a[i]);
  }
  printf("\n");
  
  err = cudaMalloc((void**)&d_a, sizeof(int) * count);
  if(err != cudaSuccess){
    printf("Cuda failed to malloc: %s\n", cudaGetErrorString(err));
  }

  err = cudaMalloc((void**)&d_b, sizeof(int) * count);
  if(err != cudaSuccess){
    printf("Cuda failed to malloc: %s\n", cudaGetErrorString(err));
  }
  
  CUDA_CHECK_ERROR("Cuda failed to malloc");

  err = cudaMemcpy(d_a, h_a, sizeof(int) * count, cudaMemcpyHostToDevice);
  if(err != cudaSuccess){
    printf("%i: Cuda failed to memcpy: %s\n", __LINE__, cudaGetErrorString(err));
  }
  CUDA_CHECK_ERROR("Cuda failed to memcpy");

  kernel_double<<<1, count>>>(d_b, d_a);
  
  err = cudaMemcpy(h_b, d_b, sizeof(int) * count, cudaMemcpyDeviceToHost);
  if(err != cudaSuccess){
    printf("%i: Cuda failed to memcpy: %s\n", __LINE__, cudaGetErrorString(err));
  }
  CUDA_CHECK_ERROR("Cuda failed to memcpy");
  
  for(int i = 0; i < count; ++i){
    printf(" %i ", h_b[i]);
  }
  printf("\n");
  
  free(h_a);
  free(h_b);
  cudaFree(d_a);
  cudaFree(d_b);
  CUDA_CHECK_ERROR("Cuda failed to free");
}
