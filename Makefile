# Acknowledgement: Functionality for creating make rules of dependencies is
# based on code presented here <http://codereview.stackexchange.com/q/11109>

CC = gcc  # To specify compiler use: '$ CC=clang make clean all'
NCC = nvcc
OPTIMIZATION_LEVEL = -O0
CFLAGS = -fno-tree-vectorize -march=armv8-a -Wall -g
LDFLAGS = -lm
# Use the compiler to generate make rules. See gcc manual for details.
MFLAGS = -MMD -MP -MF
NCFLAGS = -g -ccbin g++ -m64 -gencode arch=compute_53,code=sm_53 -gencode arch=compute_60,code=sm_60 -gencode arch=compute_62,code=sm_62 -gencode arch=compute_62,code=compute_62

SOURCES = $(wildcard *.c)
CUDA_SOURCES = $(wildcard *.cu)
OBJECTS = $(SOURCES:.c=.o) $(CUDA_SOURCES:.cu=.o)
DEPENDENCIES = $(addprefix .,$(SOURCES:.c=.d))  # Add dot prefix to hide files.

.PHONY: clean clean_obj all

all: c63enc c63dec c63pred

# IMPORTANT: When you create new files, you need to add the object version of the source file to the targets below.
c63enc: c63enc.o dsp.o tables.o io.o c63_write.o common.o me.o cuda_dsp.o cuda_common.o cuda_me.o cuda_helloworld_test.o
	$(NCC) $^ $(OPTIMIZATION_LEVEL) $(NCFLAGS) $(LDFLAGS) -rdc=true -o $@
c63dec: c63dec.c dsp.o tables.o io.o common.o me.o cuda_dsp.o cuda_common.o cuda_me.o
	$(NCC) $^ $(OPTIMIZATION_LEVEL) $(NCFLAGS) $(LDFLAGS) -rdc=true -o $@
c63pred: c63dec.c dsp.o tables.o io.o common.o me.o cuda_dsp.o cuda_common.o cuda_me.o
	$(NCC) $^ -DC63_PRED $(OPTIMIZATION_LEVEL) $(NCFLAGS) $(LDFLAGS) -rdc=true -o $@
c63enc_no_cuda: c63enc.c dsp.o tables.o io.o c63_write.o common.o me.o cuda_dsp.o cuda_common.o cuda_me.o cuda_helloworld_test.o
	$(NCC) $^ -DNO_CUDA $(OPTIMIZATION_LEVEL) $(NCFLAGS) $(LDFLAGS) -o $@

%.o: %.cu
	$(NCC) $(OPTIMIZATION_LEVEL) $(NCFLAGS) -dc $< -o $@
%.o: %.c
	$(CC) $(OPTIMIZATION_LEVEL) $(CFLAGS) $(MFLAGS) $(addprefix .,$(patsubst %.o,%.d,$@)) -c $< -o $@

clean:
	$(RM) c63enc c63dec c63pred c63enc_no_cuda $(OBJECTS) $(DEPENDENCIES)

clean_obj:
	$(RM) $(OBJECTS) $(DEPENDENCIES)

-include $(DEPENDENCIES)
