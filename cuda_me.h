#ifndef C63_CUDA_ME_H_
#define C63_CUDA_ME_H_

#include "c63.h"

void gpu_c63_motion_estimate(struct c63_common *cm);

void gpu_c63_motion_compensate(struct c63_common *cm);

#endif /* C63_CUDA_ME_H_ */
