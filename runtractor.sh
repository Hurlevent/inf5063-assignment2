#!/bin/bash


if [ $# -eq 0 ]; then
echo "Encoding tractor with cuda version"
./c63enc -w 1920 -h 1080 -f 10 -o output.y4m tractor.yuv
elif [ $1 == "nvprof" ]; then
echo "Using nvprof while encoding tractor.yuv"
nvprof ./c63enc -w 1920 -h 1080 -o output.y4m tractor.yuv
elif [ $1 == "memcheck" ]; then
cuda-memcheck ./c63enc -w 1920 -h 1080 -o output.y4m tractor.yuv
elif [ $1 == "no_cuda" ]; then
echo "Encoding tractor with cudaless version"
./c63enc_no_cuda -w 1920 -h 1080 -o output.y4m tractor.yuv
elif [ $1 == "help" ]; then
echo "Usage:"
echo "Encode tractor with cuda version"
echo "Required files: c63enc tractor.yuv"
echo "./runtractor.sh"
echo ""
echo "Use nvprof when encoding tractor"
echo "Required files: c63enc tractor.yuv"
echo "./runtractor.sh nvprof"
echo ""
echo "Encode tractor with cudaless version"
echo "Required files: c63enc_no_cuda tractor.yuv"
echo "./runtractor.sh no_cuda"
echo ""
echo "To generate the binary files, you can type:"
echo ""
echo "make"
echo ""
echo "Which will generate the c63enc binary (as well as the c63dec and c63pred)"
echo "In order to generate the cudaless version, you need to type:"
echo ""
echo "make c63enc_no_cuda"
echo ""
echo "Decode the encoded video file"
echo "Required files: c63dec output.y4m"
echo "./runtractor.sh decode"
echo ""
elif [ $1 == "decode" ]; then
echo "Decoding output.y4m"
echo "Output file: decoded.y4m"
./c63dec output.y4m decoded.y4m
fi
